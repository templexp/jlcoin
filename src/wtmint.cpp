// Copyright (c) 2014-2015 The JLCoin developers
// Distributed under the MIT/X11 software license, see the accompanying
// file COPYING or http://www.opensource.org/licenses/mit-license.php.

#include "wtmint.h"
#include "main.h"
#include "uint256.h"

/*
int64 GetBlockValue(){
		return 0;	
	}
*/
namespace WTMint
{
 

	int64 GetBlockValue(int diffTime,int nBits,int nHeight, int64 nFees, uint256 prevHash)
	{
			
				//Temple - diffblock coin implmented base on difficulty 
				if (nHeight==0)
					return 0;
				
				if (nHeight==1)
					return  8232000000000000; //5,000,000 JLCoin for about 10 years
					
				int64 nSubsidy = 16 * 100000000;
				int halvings = nHeight / 52560 ;   //262800 mins for half year, 5 min each = 52560

				// Force block reward to zero when right shift is undefined.
				if (halvings >= 64)
					return nFees;

				// Subsidy is cut in half every 52560 blocks which will occur approximately every half year.
				nSubsidy >>= halvings;

				return nSubsidy + nFees;
	}
}		 
				 