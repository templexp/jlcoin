// Copyright (c) 2016-2016 The JLC developers
// Distributed under the MIT/X11 software license, see the accompanying
// file COPYING or http://www.opensource.org/licenses/mit-license.php.


#ifndef WTMINT_H
#define WTMINT_H
 
#define TO_GENESIS_BLOCK		false

#define CLIENT_VERSION_MAJOR       1
#define CLIENT_VERSION_MINOR       10
#define CLIENT_VERSION_REVISION    5
#define CLIENT_VERSION_BUILD       5

#define WTMINT_MAX_MONEY			  8400000000000000
                                   // 271828182845904523   
#define WTMINT_MAX_SEND				  429496729600000000
								   
//#define WTMINT_GENESIS_BLOCK	"0xc741436d354a9fb337fbd79e4b2750732571f00e8b78d04493f9e9283cf4ad0b" //keep the same length

#define WTMINT_COINBASE_MATURITY 120


#define WTMINT_GENESIS_BLOCK	"0x96d5335d634e727af4b8c4d6072e8603f00628a7a49714e22e8126b49236a853"
#define WTMINT_pszTimestamp		"JW Coin By WTMINT"
#define WTMINT_scriptPubKey		"04678afdb0fe5548271967f1a67130b7105cd6a828e03909a67962e0ea1f61deb649f6bc3f4cef38c4f35504e51ec112de5c384df7ba0b8d578a4c702b6bf11d5f"

#define WTMINT_BLOCK_nTime			1468002036
#define WTMINT_BLOCK_nNonce			1627277
#define WTMINT_BLOCK_hashMerkleRoot	"0xc9777da14e7807e1cc41677df8b21c80a82c6aedb9ab9d288f3fcb84730d1e24"

#define WTMINT_BLOCK_nTargetTimespan 14400  //4 * 60 * 60 //  : every 4 hours
#define WTMINT_BLOCK_nTargetSpacing 300 //  : 1 minutes = 60


//Magic Header
#define WTMINT_MAGIC_1	0xa1
#define WTMINT_MAGIC_2	0xb6
#define WTMINT_MAGIC_3	0xc7
#define WTMINT_MAGIC_4	0xd8

//Wallet starting letter
#define WTMINT_PUBKEY_ADDRESS 43 // Start with 'J'

#define WTMINT_RPC_PORT 28761
#define WTMINT_SERVER_PORT 21678
#define WTMINT_RPC_SUBJECT "JLC"

#define WTMINT_AUX_ChainID 0x0078  //Hex
#define WTMINT_AUX_StartBlock 3
#define WTMINT_KGW_StartBlock 6000


#define WTMINT_CHKPNT_LAST_TIMESTAMP 1414056601    // * UNIX timestamp of last checkpoint block
#define WTMINT_CHKPNT_TX_QUANTITY 154911		// * total number of transactions between genesis and last checkpoint  (the tx=... number in the SetBestChain debug.log lines)
#define WTMINT_CHKPNT_ESTIMATED_TX 1500   // * estimated number of transactions per day after checkpoint
 
////////////////////////////////////////////
#include "uint256.h"
#include <boost/assign/list_of.hpp> // for 'map_list_of()'
#include <map>

//////////////////////////////////////
typedef long long  int64;
typedef unsigned long long  uint64;
typedef std::map<int, uint256> MapCheckpoints;


//////////////////////////////////////
		
class uint256;

 
namespace WTMint
{
   
    int64 GetBlockValue(int diffTime,int nBits,int nHeight, int64 nFees, uint256 prevHash);
	
	static MapCheckpoints  mapCheckpoints = boost::assign::map_list_of

        (  0, uint256("0x96d5335d634e727af4b8c4d6072e8603f00628a7a49714e22e8126b49236a853"))
        (  211, uint256("0x04a8783cd9cada715ed9f7cebe5f9cc3777b931da6f76941cb5f234a9f124a01"))
        (  19696, uint256("0x19bee618c6872ba59b9b88458e35c81720ac3dc9a71a3033d857aee7a3edfe9c"))
        (  22472, uint256("0x27ae4cfc53fcaf8204bef36c3f2ab25fa0cc46f86260e5f63c4337f8d98566a8"))
        
	;

}



#endif

